\babel@toc {english}{}
\contentsline {section}{\numberline {1}Motivation}{1}% 
\contentsline {section}{\numberline {2}The forward problem}{2}% 
\contentsline {subsection}{\numberline {2.1}The wave equation}{2}% 
\contentsline {subsection}{\numberline {2.2}The discretization of the wave equation}{2}% 
\contentsline {subsection}{\numberline {2.3}Solution of the wave equation}{2}% 
\contentsline {subsubsection}{\numberline {2.3.1}Generating the artificial data}{3}% 
\contentsline {subsubsection}{\numberline {2.3.2}Storing the generated data}{3}% 
\contentsline {subsubsection}{\numberline {2.3.3}Superposition principle}{4}% 
\contentsline {subsection}{\numberline {2.4}The discrete observation model}{4}% 
\contentsline {section}{\numberline {3}The inverse problem}{5}% 
\contentsline {subsection}{\numberline {3.1}The Bayesian Approach}{5}% 
\contentsline {subsection}{\numberline {3.2}Different prior choices}{7}% 
\contentsline {subsection}{\numberline {3.3}Reconstruction of the initial pressure}{7}% 
\contentsline {section}{\numberline {4}Optimal sensor design}{9}% 
\contentsline {subsection}{\numberline {4.1}Motivation}{9}% 
\contentsline {subsection}{\numberline {4.2}Weighting of the sensors}{10}% 
\contentsline {subsection}{\numberline {4.3}Optimization problem}{11}% 
\contentsline {subsubsection}{\numberline {4.3.1}Formulation of the optimization problem}{11}% 
\contentsline {subsubsection}{\numberline {4.3.2}Penalty function}{11}% 
\contentsline {subsubsection}{\numberline {4.3.3}Implementation overview}{13}% 
\contentsline {subsubsection}{\numberline {4.3.4}Computation speed up}{13}% 
\contentsline {subsubsection}{\numberline {4.3.5}First optimization results}{14}% 
\contentsline {subsubsection}{\numberline {4.3.6}Optimization with Trace Estimator}{15}% 
\contentsline {subsubsection}{\numberline {4.3.7}Influence of the control parameter}{16}% 
\contentsline {section}{\numberline {5}Conclusion}{17}% 
